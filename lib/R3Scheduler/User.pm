use v6.c;

use DBIx::NamedQueries;

my $version = '0.0.1';

class R3Scheduler::User {
    
    has DBIx::NamedQueries $.named_queries is required;

    method insert(Hash:D $parms){
        $!named_queries.write('user/insert',{
            name => $parms{'name'},
            description => $parms{'description'},
            quantity => 1e1.rand,
            price => 1e3.rand,
        });
    }

    method list_by_description(Str:D $description){
        $!named_queries.read: 'user/list', {
            description  => $description,
        };
    }
}
